LIS 4381

Lu Wei Huang

### Assignment # Requirements:

[A1 Subdirectory](a1/README.md "A1 Coursework")

1. Distributed Version Control with Git and Bitbucket

2. Development instalations

3. Chapter Questions (Chs 1, 2)

[A2 Subdirectory](a2/README.md "A2 Coursework")

1.	Course title, your name, assignment requirements

2.	Create an app with Healthy recipe

3.  Screenshot of application home screen;

4. 	Screenshot of running application’s second user interface;


[A3 Subdirectory](a3/README.md "A3 Coursework")

1.  Create app for concert ticket and ticket total prices

2.	Screenshot of ERD;

3.	Screenshot of running application’s first user interface

4.	Screenshot of running application’s second user interface


[A4 Subdirectory](a4/README.md "A4 Coursework")

1. Create Local host website

2. Screen shot of the carocell

3. Screen Shot of failed test

4. Screen Shot of passed test

[A5 Subdirectory](a5/README.md "A5 Coursework")

1. Course title, your name, assignment requirements, as per A1

2. Screenshots as per below examples

3. Link to local lis4381 web app: http://localhost/repos/lis4381/

[P1 Subdirectory](p1/README.md "P1 Coursework")

1. Create an app similar to that of healthy recipe, but for my own personal business card

2.	Screenshot of running application’s first user interface, which is the home page

3.	Screenshot of running application’s second user interface, which is the page with some interests

[P2 Subdirectory](p2/README.md "P2 Coursework")

1. Course title, your name, assignment requirements

2. Screenshots index.php, edit petstore, edit petstore process, carousel

3. Screen shot RSS feed

3. Link to local lis4381 web app: http://localhost/repos/lis4381/


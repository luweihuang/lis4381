> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Application Development

## Lu Wei Huang

### Assignment 5 # Requirements:

1. Course title, your name, assignment requirements, as per A1;

2. Screenshots as per below examples;

3. Link to local lis4381 web app: http://localhost/repos/lis4381/


#### README.md file should include the following items:

1. Provide Bitbucket read-only access to lis4381 repo (language PHP), include links to the other assignment repos you created in README.md, using Markdown syntax (README.md must also include screenshots as per above.) 

2. Blackboard Links: lis4381 Bitbucket repo


> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git rm - Remove files from the working tree and from the index

#### Assignment Screenshots:

*Screenshot of index.php*:

![First Screenshot](img/first.png)

*Screenshot of add_petstore_process.php (that includes error.php)*:

![Error page Screenshot](img/second.png)


*Local LIS4381 Web App:*
[Local Host](http://localhost/lis4381/"Localhost")
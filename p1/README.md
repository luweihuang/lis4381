> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Application Development

## Lu Wei Huang

### Project 1 # Requirements:

*Four Parts*

1.	Create a launcher icon image and display it in both activities (screens) 

2.	Must add background color(s) to both activities 

3.	Must add border around image and button 

4.	Must add text shadow (button) 


#### README.md file should include the following items:

1.	Course title, your name, assignment requirements, as per A1

2.	Screenshot of running application’s first user interface, which is the home page

3.	Screenshot of running application’s second user interface, which is the page with some interests


> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git rm - Remove files from the working tree and from the index

#### Assignment Screenshots:

*Screenshot of first user interface running*:

![1st User Interface Screenshot](img/businesscard1.png)

*Screenshot second user interface running*:

![2nd User Interface Screenshot](img/businesscard2.png)

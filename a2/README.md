> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Application Development

## Lu Wei Huang

### Assignment2 # Requirements:

*Three Parts*

1.	Course title, your name, assignment requirements, as per A1; 

2.	Screenshot of running application’s first user interface; 

3.	Screenshot of running application’s second user interface;

4. Chapter Questions;


#### README.md file should include the following items:

1.	Provide Bitbucket read-only access to lis4381 repo, include links to the repos you created in the above tutorials in README.md, using Markdown syntax (README.md must also include screenshots as per above.) 

2.	Blackboard Links: lis4381 Bitbucket repo 


> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git rm - Remove files from the working tree and from the index

#### Assignment Screenshots:

*Screenshot of first user interface running*:

![1st User Interface Screenshot](img/firstscreenshot.png)

*Screenshot second user interface running*:

![2nd User Interface Screenshot](img/secondscreenshot.png)
